CPUnetLOG - Display, log and plot CPU utilization and network throughput.
================================================================================
CPUnetLOG is an open source software that can:

- Display
- Log
- Plot

CPU utilization and network throughput.

![CPUnetLOG screenshot](screenshot.png)


Installation of CPUnetLOG
--------------------------------------------------------------------------------
* system-wide installation (from PIP):
    * sudo pip3 install cpunetlog
* local installation (from PIP) (places binary in ~/.local/bin --> check your $PATH):
    * pip3 install --user cpunetlog
* system-wide installation (from source repo):
    * sudo pip3 install .
* local installation (from source repo) (places binary in ~/.local/bin --> check your $PATH):
    * pip3 install --user .


Running CPUnetLOG
--------------------------------------------------------------------------------
* ./cpunetlog.py OR (in source repo)
* cpunetlog (after installation via pip)


Requirements
--------------------------------------------------------------------------------
CPUnetLOG is based on Python3.

Requires the following python3 modules:
- psutil
- netifaces

Installation on Ubuntu 16.04:
```bash
sudo apt-get install python3
sudo apt-get install python3-psutil
sudo apt-get install python3-netifaces
```
