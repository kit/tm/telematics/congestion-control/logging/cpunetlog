#!/usr/bin/env python3
# -*- coding: utf-8 -*-

"""Convenience wrapper for running CPUnetLOG directly from source tree."""

import sys
sys.settrace

from cpunetlog.main import main

if __name__ == '__main__':
    main()
